package com.example.katas;

import java.util.Arrays;
import java.util.Collections;

public class Kapreka {
    public void kaprekar(Integer intiNumber, int count){
        var startNumber = intiNumber;

        //Convert int to string.
        String temp = Integer.toString(intiNumber);

        //Convert string to an Interger[].
        Integer[] numbers = new Integer[temp.length()];
        for (int i = 0; i < temp.length() ; i++) {
            numbers[i] = temp.charAt(i) - '0';
        }

        //Sort the number array with smallest to largest.
        Arrays.sort(numbers);

        //Save the new sorted array.
        Integer[] smallToBig = numbers;

        //Start a new class that will convert the Array to a number.
        var smallToBigValue = intArrayToNumber(smallToBig);

        //Sort the number array with largest to smallest.
        Arrays.sort(numbers, Collections.reverseOrder());

        //Save the new sorted array.
        Integer[] bigToSmall = numbers;

        //Start a new class that will convert the Array to a number.
        var bigToSmallValue = intArrayToNumber(bigToSmall);

        //Check the diffrence between the numbers
        var diff = bigToSmallValue - smallToBigValue;

        //Increase the number of times the function has been started.
        count++;

        //Compare if the diffrence is the same as the starting number. If so end else start it from the top.
        if(diff == startNumber){
            System.out.println(diff + " It took " + (count - 1) + " to reach the end");
        } else {
            kaprekar(diff, count);
        }
    }
    public int intArrayToNumber(Integer[] integarArray){
        //Set initial value
        int res = 0;

        //Loop through the array and update the res with the combined number. The * is to decide the position
        for (int i = 0; i < integarArray.length; i++) {
            res=res*10+integarArray[i];
        }

        //Return the number
        return res;
    }
}
