package com.example.katas;

public class Kata {
    public static void main(String[] args) {
        Kapreka kapreka = new Kapreka();
        kapreka.kaprekar(6621, 0);
        kapreka.kaprekar(6554, 0);
        kapreka.kaprekar(1234, 0);
    }

}

/*

Today's Kata: Kaprekar
6174 is known as one of Kaprekar's constants, after the Indian mathematician D. R. Kaprekar.
Number 6174 is notable for the following rule:
Take any four-digit number, using at least two different digits (leading zeros are allowed).
Arrange the digits in descending and then in ascending order to get two four-digit numbers.
Subtract the smaller number from the bigger number.
Go back to step 2 and repeat.
The above process, known as Kaprekar's routine, will always reach its fixed point, 6174, in at most 7 iterations. Once 6174 is reached, the process will continue yielding 7641 – 1467 = 6174.
For example, choose 3524:
5432 – 2345 = 3087
8730 – 0378 = 8352
8532 – 2358 = 6174
7641 – 1467 = 6174
Write a function that will return the number of times it will take to get from a number to 6174 (the above example would equal 3).
(1)5432 – 2345 = 3087,
(2)8730 – 0378 = 8352,
(3)8532 – 2358 = 6174
Examples
kaprekar(6621) ➞ 5
kaprekar(6554) ➞ 4
kaprekar(1234) ➞ 3
*/
